import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { expect } from "chai";
import { ethers } from "hardhat";
import { DefaultEventTickets } from "../typechain";

describe("DefaultEventTickets Contract", function () {
    let DefaultEventTickets;
    let defaultEventTickets: DefaultEventTickets;
    let owner: SignerWithAddress;
    let addr1: SignerWithAddress;
    let addr2: SignerWithAddress;
    let addrs: SignerWithAddress[];

    const ticketPrice = 1;

    beforeEach(async () => {
        DefaultEventTickets = await ethers.getContractFactory("DefaultEventTickets");
        [owner, addr1, addr2, ...addrs] = await ethers.getSigners();
        defaultEventTickets = (await DefaultEventTickets.deploy("descripcion", "url", 100)) as DefaultEventTickets;
    });

    it('should be deployed', () => {
        expect(defaultEventTickets).to.be.not.null;
    })

    it('should set right owner', async () => {
        expect(await defaultEventTickets.owner()).to.equal(owner.address);
    })

    it('should buyTickets', async () => {
        const beforeBalance = await addr1.getBalance();
        defaultEventTickets.connect(addr1).buyTickets(1);
        const afterBalance = await addr1.getBalance();
        expect(afterBalance.lt(beforeBalance)).to.equal(false);
    });

    it('should not buyTickets if event already closed', async () => {
        await defaultEventTickets.connect(owner).endSale();
        await expect(defaultEventTickets.connect(addr2).buyTickets(1, {value: 100})).to.be.revertedWith('Event already closed');
    });

    it("should not buyTickets if doest`n have funds", async() => {
        await expect(defaultEventTickets.connect(addr2).buyTickets(1, {value: 1})).to.be.revertedWith('Funds not enough');
        // await expect(defaultEventTickets.buyTickets(1, {from: "addr1", value: ticketPrice - 1})).to.be.revertedWith('Funds not enough');
    })

    it('should refund Tickets', async () => {
        const beforeBalance = await addr1.getBalance();
        defaultEventTickets.connect(addr1).buyTickets(1);
        defaultEventTickets.connect(addr1).getRefund(1);
        const afterBalance = await addr1.getBalance();
        expect(afterBalance).to.equal(beforeBalance);
    });

    it('should not refund Tickets if haven`t bought any', async () => {
        await expect (defaultEventTickets.connect(addr2).getRefund(1)).to.be.revertedWith('Does not own any tickets');
    });

    it('should endSale', async () => {
        // const beforeBalance = await owner.getBalance();
        await defaultEventTickets.connect(owner).endSale();
        // const afterBalance = await owner.getBalance();
        const eventDetails = defaultEventTickets.readEvent();
        await expect((await eventDetails).isOpen).to.equal(false);
    });

    it('should not endSale if not owner', async () => {
        await expect(defaultEventTickets.connect(addr1).endSale()).to.be.revertedWith('Not owner');
    });

    it('should not endSale if already closed', async () => {
        await defaultEventTickets.connect(owner).endSale();
        await expect(defaultEventTickets.connect(owner).endSale()).to.be.revertedWith('Event already closed');
    });

});